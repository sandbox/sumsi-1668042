<?php

/**
 * Form callback for translating a job item.
 */
function tmgmt_local_translation_form($form, $form_state, TMGMTJobItem $item) {
  // We store the item in the root of the form state so we can easily access it
  // in all the form functions.
  $form_state['item'] = $item;

  $form['translation'] = array(
    '#type' => 'container',
  );

  // Build the translation form.
  $data = $item->getData();

  // Need to keep the first hierarchy. So flatten must take place inside
  // of the foreach loop.
  foreach (element_children($data) as $key) {
    $flattened = tmgmt_flatten_data($data[$key], $key);
    $form['translation'][$key] = tmgmt_local_translation_form_element($flattened, $item);
  }

  // Add the form actions as well.
  $form['actions']['#type'] = 'actions';
  $form['actions']['save'] = array(
    '#type' => 'submit',
    '#value' => t('Save translation'),
  );

  return $form;
}

/**
 * Form submit callback for the translation form.
 */
function tmgmt_local_translation_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $item = $form_state['item'];

  // Write the translated data into the job item.
  foreach (tmgmt_flatten_data($item->getData()) as $key => $data) {
    $key = str_replace('][', '|', $key);
    if (is_array($values[$key]) && isset($values[$key]['translation'])) {
      $status = !empty($values[$key]['finished']) ? TMGMT_DATA_ITEM_STATE_TRANSLATED : TMGMT_DATA_ITEM_STATE_PENDING;
      $update['#status'] = $status;
      $update['#translation'] = array(
        '#text' => $values[$key]['translation'],
      );

      $item->updateData($key, $update);
    }
  }

  // Set the job item to 'needs review' if all data items have been translated.
  if (tmgmt_local_translation_finished($item)) {
    $item->needsReview();

    // Once the job item is set to 'needs review' it is no longer accessible
    // through the translation user interface.
    $form_state['redirect'] = 'translate';
  }
  else {
    $item->save();
  }
}

/**
 * Builds a translation form element.
 */
function tmgmt_local_translation_form_element($data) {
  foreach (element_children($data) as $key) {
    if (isset($data[$key]['#text']) && _tmgmt_filter_data($data[$key])) {
      $formkey = str_replace('][', '|', $key);
      $finished =  isset($data[$key]['#status']) && $data[$key]['#status'] == TMGMT_DATA_ITEM_STATE_TRANSLATED;

      $form[$formkey] = array(
        '#tree' => TRUE,
      );

      $form[$formkey]['source'] = array(
        '#type' => 'item',
        '#title' => t('Source'),
        '#markup' => $data[$key]['#text'],
      );

      $form[$formkey]['translation'] = array(
        '#type' => 'textarea',
        '#title' => t('Translation'),
        '#default_value' => isset($data[$key]['#translation']['#text']) ? $data[$key]['#translation']['#text'] : '',
      );

      $form[$formkey]['finished'] = array(
        '#type' => 'checkbox',
        '#title' => t('Finished'),
        '#default_value' => $finished,
      );
    }
  }

  return $form;
}