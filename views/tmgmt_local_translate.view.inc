<?php

$view = new view();
$view->name = 'tmgmt_local_translate';
$view->description = 'Shows a list of translatable objects.';
$view->tag = 'default';
$view->base_table = 'tmgmt_job_item';
$view->human_name = 'Translate';
$view->core = 7;
$view->api_version = '3.0';
$view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

/* Display: Master */
$handler = $view->new_display('default', 'Master', 'default');
$handler->display->display_options['title'] = 'Translate';
$handler->display->display_options['access']['type'] = 'perm';
$handler->display->display_options['access']['perm'] = 'provide translation services';
$handler->display->display_options['cache']['type'] = 'none';
$handler->display->display_options['query']['type'] = 'views_query';
$handler->display->display_options['exposed_form']['type'] = 'basic';
$handler->display->display_options['pager']['type'] = 'full';
$handler->display->display_options['pager']['options']['items_per_page'] = '10';
$handler->display->display_options['style_plugin'] = 'table';
$handler->display->display_options['style_options']['columns'] = array(
  'item_id' => 'item_id',
  'label' => 'label',
  'plugin' => 'plugin',
  'progress' => 'progress',
  'changed' => 'changed',
  'created' => 'created',
);
$handler->display->display_options['style_options']['default'] = '-1';
$handler->display->display_options['style_options']['info'] = array(
  'item_id' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'label' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'plugin' => array(
    'sortable' => 0,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'progress' => array(
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'changed' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
  'created' => array(
    'sortable' => 1,
    'default_sort_order' => 'asc',
    'align' => '',
    'separator' => '',
    'empty_column' => 0,
  ),
);
/* No results behavior: Global: Text area */
$handler->display->display_options['empty']['area']['id'] = 'area';
$handler->display->display_options['empty']['area']['table'] = 'views';
$handler->display->display_options['empty']['area']['field'] = 'area';
$handler->display->display_options['empty']['area']['empty'] = FALSE;
$handler->display->display_options['empty']['area']['content'] = 'There are no translatable objects for you.';
$handler->display->display_options['empty']['area']['format'] = 'filtered_html';
/* Relationship: Translation Management Job Item: Tjid */
$handler->display->display_options['relationships']['tjid']['id'] = 'tjid';
$handler->display->display_options['relationships']['tjid']['table'] = 'tmgmt_job_item';
$handler->display->display_options['relationships']['tjid']['field'] = 'tjid';
$handler->display->display_options['relationships']['tjid']['required'] = TRUE;
/* Relationship: Translation Management Job: Translator */
$handler->display->display_options['relationships']['translator']['id'] = 'translator';
$handler->display->display_options['relationships']['translator']['table'] = 'tmgmt_job';
$handler->display->display_options['relationships']['translator']['field'] = 'translator';
$handler->display->display_options['relationships']['translator']['relationship'] = 'tjid';
$handler->display->display_options['relationships']['translator']['required'] = TRUE;
/* Field: Translation Management Job Item: Translation management job item ID */
$handler->display->display_options['fields']['tjiid']['id'] = 'tjiid';
$handler->display->display_options['fields']['tjiid']['table'] = 'tmgmt_job_item';
$handler->display->display_options['fields']['tjiid']['field'] = 'tjiid';
$handler->display->display_options['fields']['tjiid']['label'] = 'ID';
/* Field: Translation Management Job Item: Label */
$handler->display->display_options['fields']['label']['id'] = 'label';
$handler->display->display_options['fields']['label']['table'] = 'tmgmt_job_item';
$handler->display->display_options['fields']['label']['field'] = 'label';
$handler->display->display_options['fields']['label']['alter']['alter_text'] = TRUE;
$handler->display->display_options['fields']['label']['alter']['make_link'] = TRUE;
$handler->display->display_options['fields']['label']['alter']['path'] = 'translate/[tjiid]';
/* Field: Translation Management Job Item: Plugin */
$handler->display->display_options['fields']['plugin']['id'] = 'plugin';
$handler->display->display_options['fields']['plugin']['table'] = 'tmgmt_job_item';
$handler->display->display_options['fields']['plugin']['field'] = 'plugin';
/* Field: Translation Management Job Item: Progress */
$handler->display->display_options['fields']['progress']['id'] = 'progress';
$handler->display->display_options['fields']['progress']['table'] = 'tmgmt_job_item';
$handler->display->display_options['fields']['progress']['field'] = 'progress';
/* Field: Translation Management Job Item: Changed */
$handler->display->display_options['fields']['changed']['id'] = 'changed';
$handler->display->display_options['fields']['changed']['table'] = 'tmgmt_job_item';
$handler->display->display_options['fields']['changed']['field'] = 'changed';
$handler->display->display_options['fields']['changed']['date_format'] = 'medium';
/* Field: Translation Management Job: Created */
$handler->display->display_options['fields']['created']['id'] = 'created';
$handler->display->display_options['fields']['created']['table'] = 'tmgmt_job';
$handler->display->display_options['fields']['created']['field'] = 'created';
$handler->display->display_options['fields']['created']['relationship'] = 'tjid';
$handler->display->display_options['fields']['created']['date_format'] = 'medium';
/* Sort criterion: Translation Management Job: Created */
$handler->display->display_options['sorts']['created']['id'] = 'created';
$handler->display->display_options['sorts']['created']['table'] = 'tmgmt_job';
$handler->display->display_options['sorts']['created']['field'] = 'created';
$handler->display->display_options['sorts']['created']['relationship'] = 'tjid';
/* Filter criterion: Translation Management Job Item: State */
$handler->display->display_options['filters']['state']['id'] = 'state';
$handler->display->display_options['filters']['state']['table'] = 'tmgmt_job_item';
$handler->display->display_options['filters']['state']['field'] = 'state';
$handler->display->display_options['filters']['state']['value'] = array(
  1 => '1',
);
/* Filter criterion: Translation Management Job: State */
$handler->display->display_options['filters']['state_1']['id'] = 'state_1';
$handler->display->display_options['filters']['state_1']['table'] = 'tmgmt_job';
$handler->display->display_options['filters']['state_1']['field'] = 'state';
$handler->display->display_options['filters']['state_1']['relationship'] = 'tjid';
$handler->display->display_options['filters']['state_1']['value'] = array(
  1 => '1',
);
/* Filter criterion: Translation Management Translator: Plugin */
$handler->display->display_options['filters']['plugin']['id'] = 'plugin';
$handler->display->display_options['filters']['plugin']['table'] = 'tmgmt_translator';
$handler->display->display_options['filters']['plugin']['field'] = 'plugin';
$handler->display->display_options['filters']['plugin']['relationship'] = 'translator';
$handler->display->display_options['filters']['plugin']['value'] = array(
  'local' => 'local',
);

/* Display: Page */
$handler = $view->new_display('page', 'Page', 'page');
$handler->display->display_options['path'] = 'translate';
$handler->display->display_options['menu']['type'] = 'normal';
$handler->display->display_options['menu']['title'] = 'Translate';
$translatables['tmgmt_local_translate'] = array(
  t('Master'),
  t('Translate'),
  t('more'),
  t('Apply'),
  t('Reset'),
  t('Sort by'),
  t('Asc'),
  t('Desc'),
  t('Items per page'),
  t('- All -'),
  t('Offset'),
  t('« first'),
  t('‹ previous'),
  t('next ›'),
  t('last »'),
  t('There are no translatable objects for you.'),
  t('Translation Management Job'),
  t('Translation Management Translator'),
  t('ID'),
  t('.'),
  t(','),
  t('Label'),
  t('translate/[tjiid]'),
  t('Plugin'),
  t('Progress'),
  t('Changed'),
  t('Created'),
  t('Page'),
);