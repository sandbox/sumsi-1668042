<?php

/**
 * @file
 * Main module file for the local translation module.
 */

/**
 * Modules should return this value from hook_tmgmt_local_translation_access()
 * to allow access to a node.
 */
define('TMGMT_LOCAL_TRANSLATION_ACCESS_ALLOW', 'allow');

/**
 * Modules should return this value from hook_tmgmt_local_translation_access()
 * to deny access to a node.
 */
define('TMGMT_LOCAL_TRANSLATION_ACCESS_DENY', 'deny');

/**
 * Modules should return this value from hook_tmgmt_local_translation_access()
 * to not affect node access.
 */
define('TMGMT_LOCAL_TRANSLATION_ACCESS_IGNORE', NULL);

/**
 * Implements hook_entity_info().
 *
 * @todo wtf? move this to the core tmgmt module.
 */
function tmgmt_local_entity_info() {
  $info['tmgmt_translator']['views controller class'] = 'EntityDefaultViewsController';

  return $info;
}

/**
 * Implements hook_menu().
 */
function tmgmt_local_menu() {
  $items['translate/%tmgmt_job_item'] = array(
    'title' => 'Translate',
    'description' => 'Provide translation services.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('tmgmt_local_translation_form', 1),
    'access callback' => 'tmgmt_local_translation_item_access',
    'access arguments' => array(1),
    'file' => 'includes/tmgmt_local.pages.inc',
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function tmgmt_local_permission() {
  $perms['provide translation services'] = array(
    'title' => t('Provide translation services'),
    'descriptions' => t('Root permission for translation access: Users with this permission are eligible to be granted translation access to a translation job.'),
  );

  return $perms;
}

/**
 * Implements hook_views_api().
 */
function tmgmt_local_views_api() {
  return array(
    'api' => 3.0,
    'path' => drupal_get_path('module', 'tmgmt_local') . '/views',
  );
}

/**
 * Implements hook_default_views().
 */
function tmgmt_local_views_default_views() {
  return _tmgmt_load_exports('tmgmt_local', 'views', 'view.inc', 'view');
}

/**
 * Implements hook_tmgmt_translator_plugin_info().
 */
function tmgmt_local_tmgmt_translator_plugin_info() {
  $info['local'] = array(
    'label' => t('Local Translator'),
    'description' => t('Allows local users to process translation jobs.'),
    'plugin controller class' => 'TMGMTLocalTranslatorPluginController',
  );

  return $info;
}

/**
 * Determine whether the current user is allowed to translate a given
 * translation job.
 *
 * @param $job
 *   The translation job to be translated.
 * @param $account
 *   (Optional) A user object representing the user that is trying to obtain
 *   translation access. Determines access for a user other than the current
 *   user.
 * @return bool
 *   TRUE if the user is allowed to translate the given translation job, FALSE
 *   otherwise.
 */
function tmgmt_local_translation_access(TMGMTJob $job, $account = NULL) {
  if (!$job->isActive()) {
    return FALSE;
  }

  $rights = &drupal_static(__FUNCTION__);

  // If no user object is supplied, the access check is for the current user.
  if (empty($account)) {
    $account = $GLOBALS['user'];
  }

  // If we've already checked access for this job and user, return from cache.
  if (isset($rights[$account->uid][$job->tjid])) {
    return $rights[$account->uid][$job->tjid];
  }

  // We grant access to the translation if both of the following conditions are
  // met:
  // - No modules say to deny access.
  // - At least one module says to grant access.
  $access = module_invoke_all('tmgmt_local_translation_access', $job, $account);
  if (in_array(TMGMT_LOCAL_TRANSLATION_ACCESS_DENY, $access, TRUE)) {
    $rights[$account->uid][$job->tjid] = FALSE;
    return FALSE;
  }
  elseif (in_array(TMGMT_LOCAL_TRANSLATION_ACCESS_ALLOW, $access, TRUE)) {
    $rights[$account->uid][$job->tjid] = TRUE;
    return TRUE;
  }

  // Lastly, check for the translation capabilities.
  $input = $job->source_language;
  $required = $job->target_language;
  $capabilities = tmgmt_local_translation_capabilities($input, $account);
  $rights[$account->uid][$job->tjid] = in_array($required, $capabilities);

  return $rights[$account->uid][$job->tjid];
}

/**
 * Determine whether the current user is allowed to translate a given
 * translation job item.
 *
 * @param $item
 *   The translation job item to be translated.
 * @param $account
 *   (Optional) A user object representing the user that is trying to obtain
 *   translation access. Determines access for a user other than the current
 *   user.
 * @return bool
 *   TRUE if the user is allowed to translate the given translation job item,
 *   FALSE otherwise.
 */
function tmgmt_local_translation_item_access(TMGMTJobItem $item, $account = NULL) {
  if (!$item->isActive() || !tmgmt_local_translation_access($item->getJob(), $account)) {
    return FALSE;
  }

  return TRUE;
}

/**
 * Determines the translation capabilities of a specific user or all users.
 *
 * @param $language
 *   The source language for which to check the translation capabilities for.
 * @param $account
 *   (Optional) A user object representing the user for whom to check the
 *   translation capabilities for.
 *
 * @return array
 *   An array of supported target languages.
 */
function tmgmt_local_translation_capabilities($language, $account = NULL) {
  $capabilities = &drupal_static(__FUNCTION__);
  $key = isset($account) ? $account->uid . ':' . $language : $language;

  if (!isset($capabilities[$key])) {
    $capabilities[$key] = module_invoke_all('tmgmt_local_translation_capabilities', $language, $account);
  }

  return $capabilities[$key];
}

/**
 * Helper function for clearing the languages cache of all local translators.
 *
 * Can be used in oder to clear the cache for supported target languages after
 * the translation capabilities of an local have changed.
 */
function tmgmt_local_clear_languages_cache() {
  foreach (tmgmt_translator_load_multiple(FALSE) as $translator) {
    if ($translator->plugin == 'local') {
      cache_clear_all('languages::' . $translator->plugin, 'cache_tmgmt');
    }
  }
}

/**
 * Determines if all data items in a translation job item are translated.
 */
function tmgmt_local_translation_finished(TMGMTJobItem $item) {
  $data = tmgmt_flatten_data($item->getData());
  $data = array_filter($data, '_tmgmt_filter_data');

  $finished = TRUE;
  foreach ($data as $item) {
    if (empty($item['#status']) || $item['#status'] == TMGMT_DATA_ITEM_STATE_PENDING) {
      $finished = FALSE;
      break;
    }
  }

  return $finished;
}
